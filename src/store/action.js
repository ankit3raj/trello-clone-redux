import * as actionTypes from "./actionTypes";

// Boards

export function fetchBoardsInit() {
  return {
    type: actionTypes.FETCH_BOARDS_INIT,
  };
}

export function fetchBoardsSuccess(boards) {
  return {
    type: actionTypes.FETCH_BOARDS_SUCCESS,
    payload: boards,
  };
}

export function fetchBoardsFail(err) {
  return {
    type: actionTypes.FETCH_BOARDS_FAIL,
    payload: err,
  };
}

export function fetchBoards(data) {
  return {
    type: actionTypes.FETCH_BOARDS,
    payload: data,
  };
}

export function createBoardSuccess(board) {
  return {
    type: actionTypes.CREATE_BOARD_SUCCESS,
    payload: board,
  };
}

export function createBoard(name) {
  return {
    type: actionTypes.CREATE_BOARD,
    payload: name,
  };
}
