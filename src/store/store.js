import { legacy_createStore as createStore, combineReducers } from "redux";
import boardReducer from "./reducers/boardReducers";
const rootReducer = combineReducers({
  boardReducer,
});
const store = createStore(rootReducer);

export default store;
