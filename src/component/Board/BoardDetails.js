import React, { Component } from "react";
import Header from "./header";
import DisplayBoard from "./displayBoard";

class BoardDetails extends Component {
  render() {
    return (
      <>
        <Header />
        <DisplayBoard />
      </>
    );
  }
}
export default BoardDetails;
