import React, { Component } from "react";
import { connect } from "react-redux";
import * as TrelloApi from "../fetchingApi";
import { Spinner } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { Box, Text, Input, Button, Flex } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from "@chakra-ui/react";
import * as actions from "../../store/action";

class DisplayBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      BoardName: "",
    };
  }
  componentDidMount() {
    this.props.fetchBoards();
  }
  handleClick = () => {
    this.setState({ isOpen: true });
  };
  handleCloseClick = () => {
    this.setState({ isOpen: false });
  };
  handlesubmitClick = () => {
    this.props.createBoard(this.state.BoardName);
    this.setState({ isOpen: false });
  };

  render() {
    console.log(this.props);

    if (this.props.isLoading) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <Spinner />
        </Flex>
      );
    }
    if (this.props.error) {
      return (
        <Flex justify="center" align="center" h="100vh">
          <Text color="#36d7b7">Internal Server Error</Text>
        </Flex>
      );
    }

    return (
      <>
        <Flex m={4}>
          <Button onClick={this.handleClick}>Create Board</Button>

          <Modal isOpen={this.state.isOpen}>
            <ModalOverlay />

            <ModalContent>
              <ModalHeader>Create Board</ModalHeader>

              <ModalBody>
                <Input
                  placeholder=" Your Board Name"
                  onChange={(e) => {
                    this.setState({ BoardName: e.target.value });
                  }}
                />
              </ModalBody>

              <ModalFooter>
                <Button
                  colorScheme="red"
                  mr={3}
                  onClick={this.handleCloseClick}
                >
                  Close
                </Button>
                <Button colorScheme="blue" onClick={this.handlesubmitClick}>
                  Create a Board
                </Button>
              </ModalFooter>
            </ModalContent>
          </Modal>
        </Flex>

        {this.props.boards.length === 0 && (
          <Text>Click on create Board to start</Text>
        )}
        <Flex border="2px solid red" wrap="wrap">
          {this.props.boards.map((item) => {
            return (
              <Link to={"/boards/" + item.id} key={item.id}>
                <Box
                  w="300px"
                  h="200px"
                  rounded="20px"
                  overflow="hidden"
                  bg="SlateBlue"
                  m={20}
                >
                  <Text align="center" color="white" mt="30%" fontWeight="900">
                    {item.name}
                  </Text>
                </Box>
              </Link>
            );
          })}
        </Flex>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    boards: state.boardReducer.boards,
    isLoading: state.boardReducer.isLoading,
    error: state.boardReducer.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchBoards: () => {
      dispatch(actions.fetchBoardsInit());
      TrelloApi.displayAllBoards()
        .then((res) => {
          dispatch(actions.fetchBoardsSuccess(res));
        })
        .catch((err) => {
          dispatch(actions.fetchBoardsFail(err));
        });
    },
    createBoard: (name) => {
      dispatch(actions.createBoard(name));
      TrelloApi.createNewBoard(name).then((res) => {
        dispatch(actions.createBoardSuccess(res));
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayBoard);
